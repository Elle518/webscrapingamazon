import scrapy
from ..items import WebScrapingAmazonItem


class AmazonBooksSpider(scrapy.Spider):
    name = 'bestseller_books_amazon'
    page_number = 2
    start_urls = ['https://www.amazon.es/gp/bestsellers/books/ref=zg_bs_pg_1?ie=UTF8&pg=1']

    def parse(self, response):
        items = WebScrapingAmazonItem()
        all_books = response.css('div.a-section.a-spacing-none.aok-relative')

        for book in all_books:
            book_number = book.xpath("./div[@class='a-row a-spacing-none aok-inline-block']/span[1]/span/text()").extract_first()
            book_title = book.xpath("./span[@class='aok-inline-block zg-item']/a/div/text()").extract_first()
            book_author = book.xpath("./span[@class='aok-inline-block zg-item']/div[1]/span/text()").extract_first()

            if book_author is None:
                book_author = book.xpath("./span[@class='aok-inline-block zg-item']/div[1]/a/text()").extract_first()

            book_price = book.xpath("./span[@class='aok-inline-block zg-item']/div/a/span/span/text()").extract_first()
            book_image_link = book.xpath("./span[@class='aok-inline-block zg-item']/a/span/div/img/@src").extract_first()

            items['book_number'] = int(book_number.lstrip("#"))
            items['book_title'] = book_title.strip()
            items['book_author'] = book_author.strip()
            items['book_price'] = book_price
            items['book_image_link'] = book_image_link

            yield items

        next_page = 'https://www.amazon.es/gp/bestsellers/books/ref=zg_bs_pg_1?ie=UTF8&pg=' \
                    + str(AmazonBooksSpider.page_number)

        if AmazonBooksSpider.page_number <= 2:
            AmazonBooksSpider.page_number += 1
            yield response.follow(next_page, callback=self.parse)
